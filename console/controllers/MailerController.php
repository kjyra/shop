<?php

namespace console\controllers;

use Yii;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of MailerController
 *
 * @author grigo
 */
class MailerController extends \yii\console\Controller {
    
    public function actionMail()
    {
        $result = Yii::$app->mailer->compose()
                ->setFrom([Yii::$app->params['senderEmail']])
                ->setTo('test@bl.qwrqwrqrq')
                ->setSubject('Тестовое письмо')
                ->send();
        var_dump($result);
    }
}
