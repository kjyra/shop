<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\models\Order */

$this->title = "Заказ № {$model->id}";
$this->params['breadcrumbs'][] = ['label' => 'Orders', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="order-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'created_at',
            'updated_at',
            'user_id',
            [
                'attribute' => 'status',
                'value' => $model->status ? '<span class="text-red">Завершен</span>' : '<span class="text-green">Новый</span>',
                'format' => 'raw',
            ],
            //'status',
            'qty',
            'total',
            'name',
            'email',
            'phone',
            'address',
            'note:ntext',
        ],
    ]) ?>
    <?php $items = $model->orderProducts; ?>
    <?php foreach($items as $item): ?>
    
        <?= $item['id'] ?>
    <br>
        <?= $item->id ?>
    <br>
        <?= $item['price'] ?>
    <br>
    <br>
    <br>
    <?php endforeach; ?>

</div>
