<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace backend\controllers;

use Yii;
use backend\models\Order;
use backend\models\Product;

/**
 * Description of MainController
 *
 * @author grigo
 */
class MainController extends AppAdminController {
    
    public function actionIndex()
    {
        $orders = Order::find()->count();
        $products = Product::find()->count();
        return $this->render('index', [
            'orders' => $orders,
            'products' => $products,
        ]);
    }
    
}
