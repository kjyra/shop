<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of AccessBehavior
 *
 * @author grigo
 */

namespace backend\controllers\behaviors;

use Yii;
use yii\base\Behavior;

class AccessBehavior extends Behavior
{
    
    public function events()
    {
        return [
            \yii\web\Controller::EVENT_BEFORE_ACTION => 'checkAccess',  
        ];
    }
    
    public function checkAccess()
    {
        if(Yii::$app->user->isGuest) {
            return Yii::$app->controller->redirect(['/auth/login']);
        }
    }
}
