<?php
return [
    'adminEmail' => 'admin@example.com',
    'supportEmail' => 'support@example.com',
    'senderEmail' => 'oc.mcdir@yandex.ru',
    'senderName' => 'Grocery store',
    'user.passwordResetTokenExpire' => 3600,
    'user.passwordMinLength' => 8,
];
