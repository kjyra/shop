<?php
/* @var $this yii\web\View */
use yii\helpers\Url;
?>

<div class="banner">
<?= $this->render('//layouts/inc/sidebar.php') ?>
    <div class="w3l_banner_nav_right">
        <section class="slider">
            <div class="flexslider">
                <ul class="slides">
                    <li>
                        <div class="w3l_banner_nav_right_banner">
                            <h3>Make your <span>food</span> with Spicy.</h3>
                            <div class="more">
                                <a href="products.html" class="button--saqui button--round-l button--text-thick" data-text="Shop now">Shop now</a>
                            </div>
                        </div>
                    </li>
                    <li>
                        <div class="w3l_banner_nav_right_banner1">
                            <h3>Make your <span>food</span> with Spicy.</h3>
                            <div class="more">
                                <a href="products.html" class="button--saqui button--round-l button--text-thick" data-text="Shop now">Shop now</a>
                            </div>
                        </div>
                    </li>
                    <li>
                        <div class="w3l_banner_nav_right_banner2">
                            <h3>upto <i>50%</i> off.</h3>
                            <div class="more">
                                <a href="products.html" class="button--saqui button--round-l button--text-thick" data-text="Shop now">Shop now</a>
                            </div>
                        </div>
                    </li>
                </ul>
            </div>
        </section>
    </div>
    <div class="clearfix"></div>
</div>

<div class="w3ls_w3l_banner_nav_right_grid">
            <h3><?= "Результаты поиска по запросу: " . $q ?></h3>
            <?php if(!empty($products)): ?>
            <div class="w3ls_w3l_banner_nav_right_grid1">
                <?php foreach($products as $product): ?>
                <div class="col-md-3 w3ls_w3l_banner_left">
                    <div class="hover14 column">
                        <div class="agile_top_brand_left_grid w3l_agile_top_brand_left_grid">
                            <?php if($product->is_offer): ?>
                            <div class="agile_top_brand_left_grid_pos">
                                <?= \yii\helpers\Html::img('@web/images/offer.png', ['alt' => 'offer', 'class' => 'img-responsive']) ?>
                            </div>
                            <?php endif; ?>
                            <div class="agile_top_brand_left_grid1">
                                <figure>
                                    <div class="snipcart-item block">
                                        <div class="snipcart-thumb">
                                            <a href="<?= \yii\helpers\Url::to(['product/view', 'id' => $product->id]) ?>">
                                                <?= \yii\helpers\Html::img("@web/products/{$product->img}", ['alt' => $product->title]) ?>
                                            </a>
                                            <p><?= $product->title ?></p>
                                            <h4>
                                                $<?= $product->price ?>
                                                <?php if((float)$product->old_price): ?>
                                                    <span>$<?= $product->old_price ?></span>
                                                <?php endif; ?>
                                            </h4>
                                        </div>
                                        <form method="post" action="<?= Url::to(['cart/add', 'id' => $product->id])?>">
                                        <div class="snipcart-details">
                                            <input type="number" class="input-qty" name="quantity" min="1" max="10" value="1">
                                            <input type="submit" name="submit" class="add-to-cart" data-id="<?= $product->id ?>">
                                        </div>
                                    </form>
                                    </div>
                                </figure>
                            </div>
                        </div>
                    </div>
                </div>
                <?php endforeach; ?>
                <div class="clearfix"> </div>
								<div class="col-md-12">
								    <?= yii\widgets\LinkPager::widget([
											'pagination' => $pages,
										])?>
								</div>
            </div>
            <?php else: ?>
                <div class="w3ls_w3l_banner_nav_right_grid1">
                    <h6>По вашему запросу не было найдено ни одного товара...</h6>
                </div>
            <?php endif; ?>
        </div>