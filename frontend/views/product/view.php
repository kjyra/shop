<?php
/* @var $this yii\web\View */
use yii\helpers\Url;

?>

<!-- products-breadcrumb -->
	<div class="products-breadcrumb">
		<div class="container">
			<ul>
				<li><i class="fa fa-home" aria-hidden="true"></i><a href="<?= Url::to(['/home']) ?>">Home</a><span>|</span></li>
				<li><?= $product->category->title . ' | ' . $product->title ?></li>
			</ul>
		</div>
	</div>
<!-- //products-breadcrumb -->
<!-- banner -->
	<div class="banner">
		<?= $this->render('//layouts/inc/sidebar.php') ?>
		<div class="w3l_banner_nav_right">
			<div class="w3l_banner_nav_right_banner3">
				<h3>Best Deals For New Products<span class="blink_me"></span></h3>
			</div>
			<div class="agileinfo_single">
                <h5><?= $product->title ?></h5>
                
				<div class="col-md-4 agileinfo_single_left">
                <?php if($product->is_offer): ?>
                    <div class="agile_top_brand_left_grid_pos">
                        <?= \yii\helpers\Html::img('@web/images/offer.png', ['alt' => 'offer', 'class' => 'img-responsive']) ?>
                    </div>
                <?php endif; ?>
                    <!-- <img id="example" src="images/76.png" alt=" " class="img-responsive" /> -->
										<?= \yii\helpers\Html::img("@web/images/{$product->img}", ['alt' => $product->title, 
										'id' => 'example']) ?>
				</div>
				<div class="col-md-8 agileinfo_single_right">
					<div class="rating1">
						<span class="starRating">
							<input id="rating5" type="radio" name="rating" value="5">
							<label for="rating5">5</label>
							<input id="rating4" type="radio" name="rating" value="4">
							<label for="rating4">4</label>
							<input id="rating3" type="radio" name="rating" value="3" checked>
							<label for="rating3">3</label>
							<input id="rating2" type="radio" name="rating" value="2">
							<label for="rating2">2</label>
							<input id="rating1" type="radio" name="rating" value="1">
							<label for="rating1">1</label>
						</span>
					</div>
					<div class="w3agile_description">
						<h4>Description :</h4>
						<p><?= $product->description ?></p>
					</div>
					<div class="snipcart-item block">
						<div class="snipcart-thumb agileinfo_single_right_snipcart">
                            <h4><?= $product->price ?>
                            <?php if((float)$product->old_price): ?><span><?= $product->old_price ?></span><?php endif; ?>
                        </h4>
						</div>

							<!-- <select id="k" class="qty">
								<option class="qty__item" value="1">1</option>
								<option class="qty__item" value="2">2</option>
								<option class="qty__item" value="3">3</option>
								<option class="qty__item" value="4">4</option>
								<option class="qty__item" value="5">5</option>
								<option class="qty__item" value="6">6</option>
								<option class="qty__item" value="7">7</option>
								<option class="qty__item" value="8">8</option>
								<option class="qty__item" value="9">9</option>
								<option class="qty__item" value="10">10</option>
							</select> -->
							<form method="post" action="<?= Url::to(['cart/add', 'id' => $product->id])?>">
								<div class="snipcart-details">
									<input type="number" class="input-qty" name="quantity" min="1" max="10" value="1">
									<input type="submit" name="submit" class="add-to-cart" data-id="<?= $product->id ?>">
								</div>
							</form>
						
						<!-- <div class="snipcart-details agileinfo_single_right_details">
							<form action="#" method="post">
								<fieldset>
									<input type="hidden" name="cmd" value="_cart" />
									<input type="hidden" name="add" value="1" />
									<input type="hidden" name="business" value=" " />
									<input type="hidden" name="item_name" value="pulao basmati rice" />
									<input type="hidden" name="amount" value="21.00" />
									<input type="hidden" name="discount_amount" value="1.00" />
									<input type="hidden" name="currency_code" value="USD" />
									<input type="hidden" name="return" value=" " />
									<input type="hidden" name="cancel_return" value=" " />
									<input type="submit" name="submit" value="Add to cart" class="button" />
								</fieldset>
							</form>
						</div> -->
					</div>
				</div>
				<div class="clearfix"> </div>
			</div>
		</div>
		<div class="clearfix"></div>
	</div>
<!-- //banner -->
<!-- brands -->
	<div class="w3ls_w3l_banner_nav_right_grid w3ls_w3l_banner_nav_right_grid_popular">
		<div class="container">
			<h3>Another products of this category</h3>
				<div class="w3ls_w3l_banner_nav_right_grid1">
                    <h6><?= $category->title ?></h6>
                    <?php foreach($products as $categoryProduct): ?>
					<div class="col-md-3 w3ls_w3l_banner_left">
						<div class="hover14 column">
						<div class="agile_top_brand_left_grid w3l_agile_top_brand_left_grid">
                        <?php if($categoryProduct->is_offer): ?>
                            <div class="agile_top_brand_left_grid_pos">
                                <?= \yii\helpers\Html::img('@web/images/offer.png', ['alt' => 'offer', 'class' => 'img-responsive']) ?>
                            </div>
                        <?php endif; ?>
							<div class="agile_top_brand_left_grid1">
								<figure>
									<div class="snipcart-item block">
										<div class="snipcart-thumb">
                                            <a href="<?= \yii\helpers\Url::to(['product/view', 'id' => $categoryProduct->id]) ?>">
                                            <?= \yii\helpers\Html::img(
                                                "@web/images/{$categoryProduct->img}", ['alt' => $categoryProduct->title, 
                                                'class' => 'img-responsive'
                                                ]
                                            ) ?>
                                        </a>
											<p><?= $categoryProduct->title ?></p>
                                            <h4><?= $categoryProduct->price ?>
                                             <?php if((float)$categoryProduct->old_price): ?><span>
                                                 <?= $categoryProduct->old_price ?>
                                            </span><?php endif; ?>
                                            </h4>
										</div>
										<a href="<?= Url::to(['cart/add', 'id' => $categoryProduct->id]) ?>" class="snipcart-details agileinfo_single_right_details">Add to cart</a>
									</div>
								</figure>
							</div>
						</div>
						</div>
                    </div>
                    <?php endforeach; ?>
					<div class="clearfix"> </div>
				</div>
		</div>
	</div>
<!-- //brands -->
<!-- newsletter -->
	<div class="newsletter">
		<div class="container">
			<div class="w3agile_newsletter_left">
				<h3>sign up for our newsletter</h3>
			</div>
			<div class="w3agile_newsletter_right">
				<form action="#" method="post">
					<input type="email" name="Email" value="Email" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Email';}" required="">
					<input type="submit" value="subscribe now">
				</form>
			</div>
			<div class="clearfix"> </div>
		</div>
	</div>
<!-- //newsletter -->
