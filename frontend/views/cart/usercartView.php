<?php
/* @var $this yii\web\View */
/* @var $model frontend\models\UserCart */
use yii\helpers\Url;
use yii\helpers\Html;
use frontend\models\UserCart;

//debug($model, 1);
?>
<?php if(isset($model) && !empty($model)): ?>
<p> 
    <a href="<?= Url::to(['/cart/deleteall']) ?>">
        Удалить все позиции.
    </a>
</p>
<p><a style="margin-left: 1000px" href="<?= Url::to(['cart/checkout']) ?>" class="btn btn-primary">Оформить заказ(<?php echo $qty ?>)</a></p>
<br>
<p>Общее кол-во товаров: <?php echo $qty ?>. Общая сумма: $<?php echo $sum ?></p>
<?php foreach($model as $product): ?>
<p>Кол-во товара: <?php echo $product->qty ?></p>
<p>название товара: <?php echo $product->product->title ?></p>
<p>сумма всего товара: <?php echo $product->sum ?>$</p>
<p>сумма одного товара: <?php echo $product->product->price ?>$</p>
<a href="<?= Url::to(['product/view', 'id' => $product->product->id]) ?>"><?= Html::img("@web/products/{$product->product->img}", ['alt' => $product->product->title]); ?></a>
 <p>
     <a href="<?= Url::to(['/cart/delete', 'id' => $product->id]) ?>">
         Удалить товар
     </a>
 </p>
 <hr>
<?php endforeach; ?>
<?php else: ?>
<p>У вас нет товаров в корзине. <a href="<?= Url::to(['/home']) ?>">Вернуться к покупкам.</a></p>
<?php endif; ?>

