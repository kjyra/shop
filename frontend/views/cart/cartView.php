<?php
/* @var $this yii\web\View */
/* @var $model Yii::$app->session */

use yii\helpers\Html;
use yii\helpers\Url;
?>

<?php if(isset($_SESSION['cart'])): ?>
        <a href="<?= Url::to(['cart/deleteall']) ?>">Удалить все товары</a>
        <p><a style="margin-left: 1000px" href="<?= Url::to(['cart/checkout']) ?>" class="btn btn-primary">Оформить заказ ($<?= $_SESSION['sum']?>)</a></p>
        <p>Общее количество выбранных товаров: <?= $_SESSION['qty']?></p>
        <p>Общая сумма: $<?= $_SESSION['sum']?></p>
        <br><br>
	<?php foreach ($_SESSION['cart'] as $product): ?>
        
            <p>Название продукта: <?= $product['title']?></p>
            <p>кол-во продукта: <?= $product['qty']?></p>
            <p>Цена одного продукта: $<?= $product['price'] ?></p>
            <p>Цена всех продуктов: $<?php $sum = $product['qty'] * $product['price']; echo $sum ?></p>
            <p><a href="<?= Url::to(['product/view', 'id' => $product['id']]) ?>"><?= Html::img("@web/images/{$product['img']}", ['alt' => $product['title']]) ?></a></p>
            <p><a href="<?= Url::to(['cart/delete', 'id' => $product['id']]) ?>">Удалить товар</a></p>
            <hr>
	<?php endforeach; ?>
<?php else: ?>
            <p>Ваша корзина пуста. <a href="<?= Url::to(['/home']) ?>">Вернуться к покупкам.</a></p>
<?php endif; ?>