<?php

namespace frontend\widgets;

use Yii;
use yii\helpers\Url;

class SearchWidget extends \yii\bootstrap\Widget
{
	
	public function run()
	{
		
	}

	public function not_ajax_not_modal()
	{
		return '<div class="product_list_header">
							<a href="' . Url::to(['cart/view']) . '" class="btn btn-primary">view your cart</a>
						</div>';

		// 			 <div class="product_list_header">  
		//          	<form action="cart/view" method="post" class="last">
		  //               <fieldset>
		  //                   <input type="hidden" name="cmd" value="_cart" />
		  //                   <input type="hidden" name="display" value="1" />
		  //                   <input type="submit" name="submit" value="View your cart" class="button" />
		  //               </fieldset>
		  //           </form>
		// 			</div>
	}

}