<?php

namespace frontend\controllers;

use Yii;
use frontend\models\Cart;
use frontend\models\Order;
use frontend\models\Product;
use frontend\models\UserCart;
use frontend\models\OrderProduct;
use yii\web\NotFoundHttpException;

class CartController extends AppController
{

    public function actionAdd($id, $qty = 1)
    {
        if(Yii::$app->request->isPost) {
            $qty = Yii::$app->request->post('quantity');
        }
        $product = Product::findOne($id);
        if(empty($product)) {
            throw new NotFoundHttpException('Такого продукта нет');
        }
        if(Yii::$app->user->isGuest) {
            Cart::addProduct($product, $qty);
        } else {
            UserCart::addProduct($product, $qty);
        }
        if(Yii::$app->request->isAjax) {
            return;
        }
        return $this->redirect(Yii::$app->request->referrer ?: Yii::$app->homeUrl);
    }
    
    public function actionCheckout()
    {
        $this->setMeta('Оформление заказа', 'заказ, оформление заказа', 'оформление заказа');
        $order = new Order();
        $orderProducts = new OrderProduct();
        $transaction = Yii::$app->getDb()->beginTransaction();
        if(Yii::$app->user->isGuest) {
            $session = Yii::$app->session;
            if($order->load(Yii::$app->request->post())) {
                $order->qty = $session['qty'];
                $order->total = $session['sum'];
                if(!$order->save() || !$orderProducts->saveSessProducts($session['cart'], $order->id)) {
                    $transaction->rollBack();
                    Yii::$app->session->setFlash('error', 'Ошибка при оформлении заказа!');
                    return $this->refresh();
                } else {
                    $transaction->commit();
//                    Yii::$app->mailer->compose()
//                        ->setFrom([Yii::$app->params['senderEmail'] => Yii::$app->params['senderName']])
//                        ->setTo($order->email)
//                        ->setSubject('Новый заказ')
//                        ->send();
                    Cart::clear($session);
                    Yii::$app->session->setFlash('success', 'Ваш заказ успешно принят!');
                    return $this->refresh();
                }
            }
            return $this->render('checkout', compact('session', 'order'));
        }
        $model = UserCart::getUserCart();
        if($order->load(Yii::$app->request->post())) {
            $order->user_id = Yii::$app->user->identity->id;
            $order->qty = UserCart::getQty();
            $order->total = UserCart::getSum();
            if(!$order->save() | !$orderProducts->saveProducts($model, $order->id)) {
                $transaction->rollBack();
                Yii::$app->session->setFlash('error', 'Ошибка при оформлении заказа!');
                return $this->refresh();
            } else {
                $transaction->commit();
//                Yii::$app->mailer->compose()
//                    ->setFrom([Yii::$app->params['senderEmail'] => Yii::$app->params['senderName']])
//                    ->setTo($order->email)
//                    ->setSubject('Новый заказ')
//                    ->setTextBody($model)
//                    ->send();
                UserCart::clear();
                Yii::$app->session->setFlash('success', 'Ваш заказ успешно принят!');
                return $this->refresh();
            }
        }
        return $this->render('userCheckout', [
            'model' => $model,
            'order' => $order
        ]);
    }
    
    public function actionChange()
    {
        $id = Yii::$app->request->get('id');
        $qty = Yii::$app->request->get('qty');
        $product = Product::findOne($id);
        
        if(empty($product)) {
            throw new NotFoundHttpException('Такого продукта нет');
        }
        if(Yii::$app->user->isGuest) {
            Cart::addProduct($product, $qty);
            return true;
        } else {
            UserCart::addProduct($product, $qty);
            return true;
        }
        return false;
    }

    public function actionView()
    {
        if(Yii::$app->user->isGuest) {
//            $session = Yii::$app->session;
//            $session->open();
            return $this->render('cartView', [
                //'model' => $session
            ]);
        } else {
            $model = UserCart::getUserCart();
            $qty = 0;
            $sum = 0;
            foreach ($model as $product) {
                $qty += $product->qty;
                $sum += $product->sum;
            }
            if(isset($_POST['del_all'])) {
                foreach ($model as $product) {
                    $this->actionDelete($product->id);
                }
            }
            return $this->render('usercartView', [
                'model' => $model,
                'qty' => $qty,
                'sum' => $sum,
            ]);
        }
    }

    public function actionDelete($id)
    {
        if(Yii::$app->user->isGuest) {
            unset($_SESSION['cart'][$id]);
            return $this->redirect(Yii::$app->request->referrer);
        } else {
            $product = UserCart::findOne($id);
            $product->delete();
            return $this->redirect(Yii::$app->request->referrer);
        }
    }
    
    public function actionDeleteall()
    {
        if(Yii::$app->user->isGuest) {
            unset($_SESSION['cart']);
            unset($_SESSION['qty']);
            unset($_SESSION['sum']);
            return $this->redirect(Yii::$app->request->referrer);
        } else {
            $model = UserCart::getUserCart();
            foreach($model as $product) {
                $this->actionDelete($product->id);
            }
            return $this->redirect(Yii::$app->request->referrer);
        }
    }

}
