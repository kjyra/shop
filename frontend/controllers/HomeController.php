<?php

namespace frontend\controllers;

use Yii;
use frontend\models\Product;

class HomeController extends AppController
{
    public function actionIndex()
    {
        //Yii::$app->view->registerMetaTag("sd");
        $offers = Product::find()->where(['is_offer' => 1])->limit(4)->all();
        return $this->render('index', compact('offers'));
    }
    

}
