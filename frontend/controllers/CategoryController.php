<?php

namespace frontend\controllers;

use Yii;
use frontend\models\Product;
use frontend\models\Category;
use yii\data\Pagination;
use yii\web\NotFoundHttpException;

class CategoryController extends AppController
{
    public function actionView($id)
    {
        $category = Category::findOne($id);
        if(empty($category)) {
            throw new NotFoundHttpException('Такой категории нет...');
        }

        $this->setMeta($category->title, $category->keywords, $category->description);

        $query = Product::find()->where(['category_id' => $id]);
        $pages = new Pagination(['totalCount' => $query->count(), 
        'pageSize' => 3, 'forcePageParam' => false, 'pageSizeParam' => false,]);
        $products = $query->offset($pages->offset)->limit($pages->limit)->all();

        return $this->render('view', compact('products', 'category', 'pages'));
    }

}
