<?php

namespace frontend\controllers;

use Yii;
use frontend\models\Product;
use frontend\models\Category;
use yii\data\Pagination;
use yii\web\NotFoundHttpException;

class ProductController extends AppController
{
    public function actionSearch()
    {
        $q = trim(Yii::$app->request->get('q'));
        $this->setMeta('Поиск: ' . $q);

        if(!$q) {
            return $this->render('search');
        }

        $query = Product::find()->where(['like', 'title', $q]);
        //debug($query->count());exit;

        $pages = new Pagination(['totalCount' => $query->count(), 
        'pageSize' => 2, 'forcePageParam' => false, 'pageSizeParam' => false]);
        
        $products = $query->offset($pages->offset)->limit($pages->limit)->all();
        
        return $this->render('search', [
            'products' => $products,
            'pages' => $pages,
            'q' => $q
        ]);
    }

    public function actionView($id)
    {
        $product = Product::findOne($id);
        if(empty($product)) {
            throw new NotFoundHttpException('Запрошенного продукта не существует');
        }

        $this->setMeta("$product->title", $product->keywords, $product->description);

        $categoryOfProduct = Category::find()->where(['id' => $product->category_id])->one();
        $products = Product::find()->where(['category_id' => $categoryOfProduct->id])->all();

        return $this->render('view', [
            'product' => $product,
            'products' => $products,
            'category' => $categoryOfProduct
        ]);
    }

}
