<?php

namespace frontend\models;

use Yii;
use yii\base\Model;
use frontend\models\interfaces\ICart;

/*
	'cart' => [
		'1' => [
			'title' => Title
			'price' => price
			'img' => img
			'qty' => 5
		],

		'2' => [
			'title' => Title2
			'price' => price2
			'img' => img2
			'qty' => 2
		],

		'3' => [
			'title' => Title3
			'price' => price23
			'img' => img3
			'qty' => 1
		]

	],
	'qty' => 10,
	'sum' => 48,
		
*/

class Cart extends Model implements ICart
{

	public static function addProduct($product, $qty = 1)
	{
		$session = Yii::$app->session;
		//$session->destroy();exit;
		$session->open();
		$qty = ($qty == '-1') ? -1 : 1;
                if(isset($_SESSION['cart'][$product->id])){
                    $_SESSION['cart'][$product->id]['qty'] += $qty;
                }else{
                    $_SESSION['cart'][$product->id] = [
                        'id' => $product->id,
                        'title' => $product->title,
                        'price' => $product->price,
                        'qty' => $qty,
                        'img' => $product->img,
                    ];
                }
                $_SESSION['qty'] = isset($_SESSION['qty']) ? $_SESSION['qty'] + $qty : $qty;
                $_SESSION['sum'] = isset($_SESSION['sum']) ? $_SESSION['sum'] + $qty * $product->price : $qty * $product->price;
                if($_SESSION['cart'][$product->id]['qty'] == 0){
                    unset($_SESSION['cart'][$product->id]);
                }
	}
        
        public static function clear($session)
        {
            $session->remove('cart');
            $session->remove('qty');
            $session->remove('sum');
        }

}