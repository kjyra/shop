<?php

namespace frontend\models\interfaces;

interface ICart
{
	public static function addProduct($product, $qty = 1);
}