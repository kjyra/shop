<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "category".
 *
 * @property int $id
 * @property int $parent_id
 * @property string $title
 * @property string|null $description
 * @property string|null $keywords
 */
class Category extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'category';
    }

    /**
     * возвращает объект родителя у вызывающего объекта по parent_id
     */
    public function getParent()
    {
        return self::find()->select('id, title, parent_id')->where(['id' => $this->parent_id])->one();
    }

    public function getParents($category)
    {
        $parents = [];
        $categoriesTitle = [];
        $categoriesTitle[] = $category->title . ' ';
        if($category->parent_id != 0) {
            while($category->parent_id != 0) {
                $parents[] = $category->getParent();
                $category = $category->getParent();
            }
            foreach($parents as $category) {
                $categoriesTitle[] = '<a href="/category/<?php echo $category->id ?>"> ' . $category->title . "</a>  ";
            }
            return $categoriesTitle;
        } else {
            return $parents;
        }
    }
    
    public function getProducts()
    {
        return $this->hasMany(Product::class, ['category_id' => 'id'])->all();
    }
}
