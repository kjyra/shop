<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "order_product".
 *
 * @property int $id
 * @property int $order_id
 * @property int $product_id
 * @property string $title
 * @property float $price
 * @property int $qty
 * @property float $total
 */
class OrderProduct extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'order_product';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['order_id', 'product_id', 'title', 'price', 'qty', 'total'], 'required'],
            [['order_id', 'product_id', 'qty'], 'integer'],
            [['price', 'total'], 'number'],
            [['title'], 'string', 'max' => 255],
        ];
    }
    
    public function saveSessProducts($products, $order_id)
    {
        foreach($products as $product) {
            $this->id = null;
            $this->isNewRecord = true;
            $this->order_id = $order_id;
            $this->product_id = $product['id'];
            $this->title = $product['title'];
            $this->price = $product['price'];
            $this->qty = $product['qty'];
            $this->total = $product['price'] * $product['qty'];
            if(!$this->save()) {
                return false;
            }
        }
        return true;
    }
    
    public function saveProducts($products, $order_id)
    {
        foreach($products as $product) {
            $this->id = null;
            $this->isNewRecord = true;
            $this->order_id = $order_id;
            $this->product_id = $product->product_id;
            $this->title = $product->product['title'];
            $this->price = $product->product['price'];
            $this->qty = $product['qty'];
            $this->total = $product['sum'];
            if(!$this->save()) {
                return false;
            }
        }
        return true;
    }
}
