<?php

namespace frontend\models;

use Yii;
use yii\helpers\Url;
use yii\db\ActiveRecord;
use frontend\models\User;
use yii\helpers\ArrayHelper;
use frontend\models\interfaces\ICart;

class UserCart extends ActiveRecord implements ICart
{

	public static function tableName()
	{
		return 'cart';
	}

	public static function addProduct($product, $qty = 1)
	{
            //$qty = ($qty == '-1') ? -1 : 1;
            $userId = Yii::$app->user->identity->id;
            $cart = UserCart::find()->where(['user_id' => $userId])->andWhere(['product_id' => $product->id])->one();
            if($cart) {
		$cart->sum += $product->price * $qty;
		$cart->user_id = $userId;
		$cart->product_id = $product->id;
		$cart->qty += $qty;
                $cart->img = $product->img;
            } else {
                $cart = new Self();
		$cart->sum = $product->price * $qty;
		$cart->user_id = $userId;
		$cart->product_id = $product->id;
		$cart->qty = $qty;
                $cart->img = $product->img;
            }
            if($cart->qty <= 0) {
                $cart->delete();
            } else {
                $cart->save();
            }
	}
        
        public static function clear()
        {
            $userCart = self::getUserCart();
            foreach($userCart as $cart) {
                $cart->delete();
            }
        }

	public function getProduct()
	{
		return $this->hasOne(Product::class, ['id' => 'product_id'])->one();
	}
        
        public static function getUserCart()
        {
            $userId = Yii::$app->user->identity->id;
            $model = UserCart::find()->where(['user_id' => $userId])->all();
            return $model;
        }

        public static function getQty()
        {
            $userCart = self::getUserCart();
            $qty = 0;
            foreach($userCart as $product) {
                $qty += $product->qty;
            }
            return $qty;
        }
        
        public static function getSum()
        {
            $userCart = self::getUserCart();
            $sum = 0;
            foreach($userCart as $product) {
                $sum += $product->sum;
            }
            return $sum;
        }
}